import json
import os

import factory

from extractor.models import SocrataCatalogItem
from extractor.tests.factories.socrata_catalog import SocrataCatalogFactory

this_dir = os.path.dirname(os.path.abspath(__file__))

fixture = json.load(open(os.path.join(
    this_dir, '../fixtures/6imu-meau.json'
)))


class SocrataCatalogItemFactory(factory.django.DjangoModelFactory):

    access_level = fixture['accessLevel']
    contact_point = fixture['contactPoint']
    description = fixture['description']
    distribution = fixture['distribution']
    identifier = fixture['identifier']
    issued = fixture['issued']
    landing_page = fixture['landingPage']
    modified = fixture['modified']
    publisher = fixture['publisher']

    title = fixture['title']
    catalog = factory.SubFactory(SocrataCatalogFactory)

    class Meta:
        model = SocrataCatalogItem
