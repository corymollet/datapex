import json
import os

import factory

from extractor.models import SocrataCatalog

this_dir = os.path.dirname(os.path.abspath(__file__))
chicago_dataset = json.load(open(os.path.join(
    this_dir, '../fixtures/chicago_dataset_fixture.json'
)))


class SocrataCatalogFactory(factory.django.DjangoModelFactory):
    """
    A factory class for creating :class:`extractor.models.SocrataCatalog` to use in tests
    """

    label = factory.Faker('city')
    context = "https://project-open-data.cio.gov/v1.1/schema/catalog.jsonld"
    uri = "https://data.cityofchicago.org/data.json"
    dcat_type = 'dcat:Catalog'
    conforms_to = "https://project-open-data.cio.gov/v1.1/schema"
    described_by = "https://project-open-data.cio.gov/v1.1/schema/catalog.json"
    dataset = chicago_dataset

    class Meta:
        model = SocrataCatalog
        django_get_or_create = ('uri',)
