import json
import os
import re

from django.test import TestCase

from extractor.tests.factories.socrata_catalog_item import SocrataCatalogItemFactory

SLUG_PATTERN = re.compile(r'\w{4}-\w{4}', re.ASCII)
this_dir = os.path.dirname(os.path.abspath(__file__))


class SocrataCatalogItemTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.dist1 = json.load(open(os.path.join(
            this_dir, 'fixtures/dist1_fixture.json'
        )))
        cls.dist4 = json.load(open(os.path.join(
            this_dir, 'fixtures/dist4_fixture.json'
        )))
        cls.dist6 = json.load(open(os.path.join(
            this_dir, 'fixtures/dist6_fixture.json'
        )))
        cls.dist8 = json.load(open(os.path.join(
            this_dir, 'fixtures/dist8_fixture.json'
        )))

    def setUp(self):
        self.catalog_item = SocrataCatalogItemFactory()

    def test_get_distribution_types(self):
        dist_types = self.catalog_item.get_distribution_types()

        self.assertIn('KMZ', dist_types)
        self.assertIn('KML', dist_types)
        self.assertIn('Shapefile', dist_types)
        self.assertIn('Original', dist_types)
        self.assertNotIn('GeoJSON', dist_types)
        self.assertNotIn('format', dist_types)
        self.assertNotIn('export', dist_types)

    def test_get_distribution_type_url(self):
        shapefile_url = self.catalog_item.get_distribution_type_url('Shapefile')
        self.assertEqual(
            shapefile_url[0],
            f'https://data.cityofchicago.org/api/geospatial/{self.catalog_item.slug}?method=export&format=Shapefile'
        )
        self.assertEqual(
            shapefile_url[1],
            'zip'
        )
        self.assertNotEqual(
            shapefile_url[0],
            f'https://data.cityofchicago.org/api/geospatial/{self.catalog_item.slug}?method=export&format=KML'
        )
        self.assertIsNone(self.catalog_item.get_distribution_type_url('GeoJSON'))

    def test_slug_looks_right(self):
        slug = self.catalog_item.slug
        self.assertTrue(slug[0])

        self.assertIsNotNone(re.search(
            SLUG_PATTERN,
            slug
        ))

    def test_has_binary_distribution(self):
        self.assertTrue(self.catalog_item.has_binary_distribution())

    def test_extract_archive(self):
        path = os.path.join(this_dir, 'fixtures/Hospitals.zip')
        expected_file_list = [
            'Hospitals.dbf',
            'Hospitals.prj',
            'Hospitals.sbn',
            'Hospitals.sbx',
            'Hospitals.shp',
            'Hospitals.shx'
        ]
        extracted_files = self.catalog_item.extract_zip(
            path,
            extract_dir=os.path.join(this_dir, 'fixtures')
        )
        self.assertListEqual(sorted(expected_file_list), sorted(extracted_files))
        # clean up
        for i in expected_file_list:
            fpath = os.path.join(this_dir, 'fixtures/{}'.format(i))
            os.unlink(fpath)
