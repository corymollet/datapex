import json
import os

from django.test import TestCase

from extractor.tests.factories.socrata_catalog import SocrataCatalogFactory

this_dir = os.path.dirname(os.path.abspath(__file__))


class SocrataCatalogTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Chicago Street Centerline GIS file
        cls.slug = '6imu-meau'
        cls.catalog_item = json.load(open(
            os.path.join(
                this_dir, 'fixtures', '{}.json'.format(cls.slug)
            )
        ))

    def test_get_from_slug(self):
        catalog = SocrataCatalogFactory()

        real_item = catalog.get_from_slug(self.slug)
        self.assertIsNotNone(real_item)
        self.assertDictEqual(self.catalog_item, real_item)

        real_item_strict = catalog.get_from_slug(self.slug)
        self.assertIsNotNone(real_item_strict)
        self.assertDictEqual(self.catalog_item, real_item_strict)

        fake_item = catalog.get_from_slug('foo')
        self.assertDictEqual(fake_item, {})

        empty_string = catalog.get_from_slug('')
        self.assertDictEqual(empty_string, {})

        null_test = catalog.get_from_slug(None)
        self.assertDictEqual(null_test, {})

    def test_create_all_items(self):
        catalog = SocrataCatalogFactory()
        catalog.create_all_items()
        self.assertEqual(
            len(catalog.dataset),
            catalog.items.count()
        )

        # testing Django get_or_create
        catalog.create_all_items()
        self.assertEqual(
            len(catalog.dataset),
            catalog.items.count()
        )


