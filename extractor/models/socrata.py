import logging
import os
import re
import typing
import zipfile
from collections import Counter

import requests
from django.conf import settings
from django.contrib.gis.db import models
from django.db.utils import IntegrityError
from django.utils import timezone
from sridentify import Sridentify

from extractor import utils
from .base import UUIDTimestampedModel

logger = logging.getLogger(__name__)


class SocrataCatalog(UUIDTimestampedModel):
    """A model to hold information about a Socrata-powered Data Catalog."""
    label = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        help_text="A human-friendly label for this dataset"
    )
    uri = models.CharField(max_length=2048, unique=True)
    context = models.CharField(max_length=255, null=True, blank=True)
    dcat_type = models.CharField(max_length=255, null=True, blank=True)
    conforms_to = models.CharField(max_length=255, null=True, blank=True)
    described_by = models.CharField(max_length=255, null=True, blank=True)
    dataset = models.JSONField(default=list)

    def __repr__(self):
        if self.label:
            return '<SocrataCatalog: {}>'.format(self.label)
        else:
            return '<SocrataCatalog: {}>'.format(self.uri)

    @classmethod
    def fetch(cls, uri=None) -> dict:
        """
        Fetch the catalog metadata.

        :param uri: The URI to fetch, if different from the ``uri`` field of an instance of this model.
        :type uri: str
        :return: Catalog metadata
        :raises: :class:`requests.exceptions.HTTPError`, if one occurs
        :raises: :class:`json.JSONDecodeError`, if the response is not valid JSON
        """
        if uri is None:
            uri = cls.uri
            if not uri.endswith('data.json'):
                uri = utils.urijoin(uri, 'data.json')
        resp = requests.get(uri)
        resp.raise_for_status()
        return resp.json()

    def get_from_slug(self, slug: str) -> dict:
        """
        Get a specific catalog item from its "slug" (Socrata unique identifier), if it exists.

        :param slug: The Socrata slug
        :return: Catalog item
        """
        item = [x for x in self.dataset
                if x.get('identifier', '').split('/')[-1] == slug]
        return next((x for x in item), {})

    def create_all_items(self) -> None:
        """Create :class:`SocrataCatalogItem` from this Catalog's dataset."""
        for item in self.dataset:
            logger.info("Indexing {}: {}".format(
                item.get('identifier'),
                item.get('title')
            ))
            try:
                catalog_item, _ = SocrataCatalogItem.objects.get_or_create(
                    access_level=item.get('accessLevel'),
                    contact_point=item.get('contactPoint'),
                    description=item.get('description'),
                    distribution=item.get('distribution', []),
                    identifier=item.get('identifier'),
                    issued=item.get('issued'),
                    landing_page=item.get('landingPage'),
                    modified=item.get('modified'),
                    publisher=item.get('publisher'),
                    title=item.get('title'),
                    catalog=self
                )
            except IntegrityError:
                # Some data catalogs have items with the same identifier field
                logger.warning(
                    "SocrataCatalogItem with identifier {} in this catalog already exists".format(
                        item['identifier']
                    )
                )

    def get_top_keywords(self, n=None) -> typing.List[typing.Tuple[str, int]]:
        """
        Get the top keywords for every dataset in the catalog.

        :param n: The top number to get (None returns all)
        :type n: int
        :return: List of top keywords
        """
        keywords = Counter([k for item in self.dataset for k in item.get('keyword', [])])
        return keywords.most_common(n)


class SocrataCatalogItem(UUIDTimestampedModel):
    """An individual record from the catalog of available datasets
    available on any Socrata-powered data portal accessible at
    <opendata-domain>/data.json.
    """
    # TODO: Assumes the format= is always at the end of the querystring; make it more robust
    DISTRIBUTION_FORMAT_PATTERN = re.compile(r'format=(\w+$)')

    access_level = models.CharField(max_length=255, null=True, blank=True)
    contact_point = models.JSONField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    distribution = models.JSONField(default=list)

    # TODO: for some catalogs this is not unique
    #  https://gitlab.com/corymollet/datapex/issues/1
    identifier = models.CharField(max_length=255, unique=True)

    issued = models.DateField(null=True, blank=True)
    landing_page = models.URLField(max_length=255, null=True, blank=True)
    modified = models.DateField(null=True, blank=True)
    publisher = models.JSONField(null=True, blank=True)
    orig_file_loc = models.CharField(max_length=255, null=True, blank=True)
    last_downloaded_on = models.DateTimeField(null=True, blank=True)
    title = models.CharField(max_length=255)
    catalog = models.ForeignKey(SocrataCatalog, on_delete=models.CASCADE, related_name='items')

    def __repr__(self):
        return '<SocrataCatalogItem: {} | {}>'.format(self.slug, self.title)

    def get_distribution_types(self) -> list:
        """Get all the format=<type> values for the distribution."""
        types = []
        for dist in self.distribution:
            search = self.DISTRIBUTION_FORMAT_PATTERN.search(
                dist.get('downloadURL', ''),
                re.IGNORECASE
            )
            if search is not None:
                types.append(search.group(1))
        return types

    def get_distribution_type_url(self, fmt: str) -> typing.Optional[typing.Tuple[str, str]]:
        """
        Get the download URL for a "distribution" for datasets
        that are not easily served up as JSON over a REST API. Typically,
        these are large files (such as geospatial files) or files in
        proprietary formats (such as Microsoft Excel or Access).

        The type of ``fmt`` to use can be determined by inspecting the result of the ``get_distribution_types``
        method of this class.

        :param fmt: A string value of the type of distribution to download.
        :return: tuple: (str: URL, str: file extension), if this item has a distribution
        """

        pattern = re.compile('format={}$'.format(fmt))
        for dist in self.distribution:
            search = pattern.search(
                dist.get('downloadURL', ''),
                re.IGNORECASE
            )
            if search is not None:
                return search.string, settings.MIME_EXTENSIONS.get(dist['mediaType'], '')

    @property
    def slug(self) -> str:
        """
        Get the slug that uniquely identifies this dataset across
        all Socrata open data portals.

        TODO: This needs to be rethought
         https://gitlab.com/corymollet/datapex/issues/1

        """
        return self.identifier.split('/')[-1]

    def has_binary_distribution(self) -> bool:
        """Does this item have a binary format as one of its distributions?"""
        return any(x.get('mediaType') in settings.BINARY_MIME_TYPES for x in self.distribution)

    @property
    def staging_dir(self) -> str:
        """The directory to store "staged" distribution files."""
        return os.path.join(settings.DATASTORE_STAGING, f'item_{self.slug}')

    def get_staging_file_list(self, full_path=True, filetype_filter=None) -> list:
        """
        Get the list of files in this item's staging directory.

        :param full_path: whether or not to return the full path to the file
        :type full_path: bool
        :param filetype_filter: optional filter for specific file types (e.g., '.xlsx' or '.shp')
        :type filetype_filter: str
        :return: List of files.
        """
        if os.path.exists(self.staging_dir):
            file_list = os.listdir(self.staging_dir)
            if filetype_filter:
                file_list = [x for x in file_list if x.endswith(filetype_filter)]
            if full_path:
                return [os.path.join(self.staging_dir, x) for x in file_list]
            else:
                return file_list
        return []

    @property
    def epsg_code(self) -> typing.Optional[int]:
        """If this dataset has a shapefile distribution, attempt to get the EPSG
        code from its .prj file."""

        prj = self.get_staging_file_list(filetype_filter='.prj')
        if prj:
            ident = Sridentify()
            ident.from_file(prj[0])
            return ident.get_epsg()

    def fetch_distribution(self, url: str, extension: str) -> None:
        """
        Download a file pointed to by one of the distribution URLs

        :param url: (str): The URL to fetch
        :param extension: (str): The file extension to use for the downloaded file
        """
        filename = f'item_{self.slug}.{extension}'
        if not os.path.exists(settings.DATASTORE_ORIG):
            os.makedirs(settings.DATASTORE_ORIG)
        path = os.path.join(settings.DATASTORE_ORIG, filename)
        req = requests.get(
            url=url,
            stream=True,
            headers={'X-App-Token': settings.SOCRATA_APP_TOKEN}
        )
        req.raise_for_status()
        with open(path, 'wb') as outfile:
            for chunk in req.iter_content(chunk_size=1024):
                if chunk:
                    outfile.write(chunk)
        self.orig_file_loc = path
        self.last_downloaded_on = timezone.now()
        self.save()

    @classmethod
    def extract_zip(cls, path: str, extract_dir: str) -> list:
        """
        Extract archive (usually .zip) downloaded distributions.

        :param path: Path to the archive file to extract
        :param extract_dir: Optional directory to extract the files
        contained in the  archive. If not provided, it will default to the
        value of DATASTORE_STAGING configured in settings.py
        :return: The list of extracted files
        """
        if not os.path.exists(extract_dir):
            os.makedirs(extract_dir)
        # TODO: Handle zipfile Exceptions
        zf = zipfile.ZipFile(path)
        zf.extractall(path=extract_dir)
        return zf.namelist()

    def _extract_zip(self):
        extract_dir = os.path.join(settings.DATASTORE_STAGING, f'item_{self.slug}')
        return self.extract_zip(self.orig_file_loc, extract_dir)
