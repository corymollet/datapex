from django.core.management.base import BaseCommand, CommandError

from extractor.models import SocrataCatalog
from extractor import utils


def catalog_exists(domain):
    return SocrataCatalog.objects.filter(uri__iexact=domain).exists()


class Command(BaseCommand):
    help = """Index and populate metadata about all datasets on a Socrata-powered data portal"""

    def add_arguments(self, parser):
        parser.add_argument(
            '-d',
            '--domain',
            help='The domain of the data portal, without the trailing slash (e.g. data.sfgov.org)',
            dest='domain',
            required=True
        ),
        parser.add_argument(
            '-l',
            '--label',
            help='An optional label to apply to this catalog (e.g. "DataSF")',
            dest='label',
            required=False
        ),
        parser.add_argument(
            '-n',
            '--no-create-items',
            help='Only download the catalog; do not create individual metadata records '
                 'about all the items in this dataset',
            dest='nocreate',
            action='store_true',
            default=False
        )

    def handle(self, *args, **options):
        # TODO: Add options for updating/refreshing?
        domain = utils.sanitize_domain(options['domain'])
        if catalog_exists(domain):
            self.stdout.write(
                self.style.WARNING(
                    f"{domain} has already been indexed!"
                )
            )
        else:
            catalog = SocrataCatalog(uri=domain)
            self.stdout.write(
                self.style.NOTICE(
                    "Attempting to download the catalog from {},\nplease wait...".format(domain)
                )
            )
            dataset = catalog.fetch(domain + '/data.json')
            catalog.label = options.get('label')
            catalog.context = dataset.get('@context')
            catalog.dcat_type = dataset.get('@type')
            catalog.conforms_to = dataset.get('conformsTo')
            catalog.described_by = dataset.get('describedBy')
            catalog.dataset = dataset['dataset']
            catalog.save()
            self.stdout.write(
                self.style.SUCCESS(
                    "Success!"
                )
            )
            if not options['nocreate']:
                self.stdout.write(
                    self.style.NOTICE(
                        'Indexing all items in the catalog...'
                    )
                )
                catalog.create_all_items()
                self.stdout.write(
                    self.style.SUCCESS(
                        "Done!"
                    )
                )
