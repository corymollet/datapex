

def sanitize_domain(domain: str) -> str:
    """
    Strip trailing slash and add https:// scheme in front of a domain name.

    :param domain: the domain name
    """
    if domain.endswith('/'):
        domain = domain[:-1]
    if not domain.startswith('http'):
        domain = f'https://{domain}'
    return domain.strip()


def urijoin(*args: str) -> str:
    """
    Construct a URI from the input str args by separating them with a slash (/).
    Slashes passed in to any element of *args will be silently discarded.

    >>> urijoin('data.sfgov.org', 'data.json')
    'data.sfgov.org/data.json'

    >>> urijoin('data.sfgov.org/', 'data.json')
    'data.sfgov.org/data.json'

    >>> urijoin('data.sfgov.org/', '/data.json')
    'data.sfgov.org/data.json'

    :param args: URI components to separate with a /
    """
    if not all(isinstance(x, str) for x in args):
        raise TypeError('all arguments passed in to this function must be strings')
    components = [x.replace('/', '').strip() for x in args]
    components = [x for x in components if x.strip()]
    uri = '/'
    uri += '/'.join(components)
    return uri


DCAT_FIELD_MAPPING = {
    '@context': 'context',
    '@type': 'dcat_type',
    'conforms_to': 'conformsTo',
    'described_by': 'describedBy'
}
