from extractor.models import SocrataCatalog


def save_catalog(uri, label=None):
    """
    Save a Socrata Catalog to the database
    :param uri: str, URI to fetch
    :param label: str, optional label
    :return: tuple, (SocrataCatalog, bool created)
    """

    response = SocrataCatalog.fetch(uri)
    return SocrataCatalog.objects.get_or_create(
        label=label,
        uri=uri,
        context=response.get('@context'),
        dcat_type=response.get('@type'),
        conforms_to=response.get('conformsTo'),
        described_by=response.get('describedBy'),
        dataset=response.get('dataset')
    )

