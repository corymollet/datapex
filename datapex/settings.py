"""
Django settings for datapex project.

Generated by 'django-admin startproject' using Django 2.0.7.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'u5-8zu29+lfj-@$&7&1f0-k$d_qr14pdug&8syd4p40+x733fz'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis.db',
    'api',
    'extractor',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'datapex.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'datapex.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.getenv('POSTGRES_DB', 'datapex'),
        'USER': os.getenv('POSTGRES_USER', 'datapex'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', 'datapex'),
        'PORT': os.getenv('POSTGRES_PORT', '5432'),
        'HOST': os.getenv('POSTGRES_HOST', 'postgis')
    }
}


# Custom User model, for more flexibility if needed later
# https://docs.djangoproject.com/en/2.0/topics/auth/customizing/#substituting-a-custom-user-model
AUTH_USER_MODEL = 'api.User'


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'

# Mapping of mime types (`mediaType` in DCAT: https://www.w3.org/ns/dcat#mediaType ) to typical file extensions
MIME_EXTENSIONS = {
    'application/json': 'json',
    'application/rdf+xml': 'rdf',
    'application/vnd.google-earth.kml+xml': 'kml',
    'application/vnd.google-earth.kmz': 'kmz',
    'application/xml': 'xml',
    'application/zip': 'zip',
    'text/csv': 'csv',
    'test/html': 'html'
    }

BINARY_MIME_TYPES = (
    'application/octet-stream',
    'application/vnd.google-earth.kmz',
    'application/zip',
)

# Filesystem directory to store downloaded distributions
DATASTORE = os.path.join(BASE_DIR, 'extractor/datastore')

# The directory in DATASTORE where original downloaded distribution
# files will be stored
DATASTORE_ORIG = os.path.join(DATASTORE, 'orig')

# The directory in DATASTORE where staged downloaded distribution
# files will be stored. Examples could be the extracted contents of a zip file or some other
# alteration of an original downloaded file before further processing, such as
# loading into a database
DATASTORE_STAGING = os.path.join(DATASTORE, 'staging')
