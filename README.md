# Datapex

Datapex (**Data P** ortal **Ex** tractor) is a GeoDjango app for automatically extracting/mirroring data from open data portals for app development and data analysis.

## Installation

### .env file

Copy `example.env` to `.env` and edit accordingly.


### Local Settings

Add a file in the `datapex` folder called `local_settings.py`. We'll store sensitive variables like API tokens here (and obviously, keep it out of version control). You can also add things to it that are specific to your setup, such as appending hosts to the `ALLOWED_HOSTS` variable, which you may need to do depending on your Docker/VM setup, or if you want to store downloaded data somewhere else (by overriding the `DATASTORE` variables from base settings).

One API token you should put here is an [app token from Socrata](https://opendata.socrata.com/login).

```py
from .settings import *

SOCRATA_APP_TOKEN = ''

ALLOWED_HOSTS += ['localhost', 'testserver', '192.168.99.100']  # Your docker-machine's IP

DATASTORE = '/path/to/my/datastore'  # If you want to change it from the default of extractor/datastore
```

### Docker

```
$ docker-compose build
$ docker-compose up
```
On first run the PostGIS docker image has to run a few bootstrap scripts to finish configuring PostGIS and its extensions. The `web` container will sometimes detect the `postgis` container as being ready before it actually is and will error out because it can't connect to the database. If this happens just watch the log output in the terminal where `docker-compose run` is running until you see the line that says the `postgis` container is "ready to accept connections," then open another terminal and run `docker-compose restart web`.

Depending on how Docker was installed and configured on your machine, you can either open up http://localhost:8000 or you'll have to find your `docker-machine`'s IP address (`docker-machine ls` should show it) to hopefully see the Django "It Worked!" placeholder. If you can't connect either via localhost or your `docker-machine IP address:8000`, try to isolate whether it's a Docker problem or Django problem:

```bash
$ docker-compose exec web bash
$ curl http://localhost:8000
# if this returns HTML that looks like the Django default "It Worked" page,
# double check your Docker setup - maybe your docker-machine IP address changed

```

Now you should be able to run migrations with `docker-compose run --rm web python manage.py migrate`

### Datastore

The default datastore (for storing downloaded data) is a folder called `datastore` in the `extractor` app. This directory is created in the Dockerfile but is kept out of version control for obvious reasons. You can change it to somewhere else, but this could get very tricky with a Docker-based setup. To change it, override the `DATASTORE` settings from `datapex/settings.py` by changing them in your `datapex/local_settings.py`.

### Run Tests

```bash
$ docker-compose run web python manage.py test
```

## Fetching Data

You can use the `index_catalog` management command to index all the datasets from a Socrata-powered data portal, e.g.:

```shell script
docker-compose run --rm web python manage.py index_catalog --domain data.cityofchicago.org --label "City of Chicago"
```
